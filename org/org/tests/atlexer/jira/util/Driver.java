package org.tests.atlexer.jira.util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Driver extends FirefoxDriver
{
    
    private static WebDriver driver = new Driver();
    
    private Driver () 
    {
    }
    
    public static WebDriver getDriver() 
    {
       driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
       return driver;
    }
}
