package org.tests.atlexer.jira.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditPage {
    private final WebDriver driver;

    public EditPage(WebDriver driver) 
    {
	this.driver = driver;
    }
    
    By editButtonLocator = By.id("edit-issue");
    By projectLocator = By.id("project-field");
    By issueTypeLocator = By.id("issuetype-field");
    By summaryLocator = By.id("summary-val");
    By assigneeLocator = By.id("assignee-field");
    By descriptionLocator = By.id("description");
    By editSubmit = By.id("edit-issue-submit");
    By alertMsg = By.partialLinkText("has been successfully created");

    public EditPage clickEdit ()
    {
	driver.findElement(editButtonLocator).click();
	return this;
    }
    public EditPage selectProject(String arg) {
	driver.findElement(projectLocator).sendKeys(arg);
	return this;    
    }

    public EditPage selectAssignee(String arg) {
	driver.findElement(assigneeLocator).sendKeys(arg);
	return this;    
    }
    public EditPage selectIssueType(String arg) {
	driver.findElement(issueTypeLocator).sendKeys(arg);
	return this;    
    }
    
    public EditPage setSummary(String arg) {
	driver.findElement(summaryLocator).sendKeys(arg);
	return this;    
    }
 
    
    public String getSummary() {
	return driver.findElement(summaryLocator).getText();
     }
    
    public String getDescription() {
	return driver.findElement(descriptionLocator).getText();
     }
    
    public EditPage setDescription(String arg) {
	driver.findElement(descriptionLocator).sendKeys(arg);
	return this;    
    }
    
    public EditPage submit()
    {
	driver.findElement(editSubmit).submit();
	return this;
    }

    public EditPage updateBug(String proj, String issueType, String summary, String assignee) 
    {
	clickEdit();
	(new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.id("summary")));	
	setSummary (summary);
	setDescription (summary);
	submit();
	return this;
    }
}