package org.tests.atlexer.jira.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
    private final WebDriver driver;

    public LoginPage(WebDriver driver) {
	//driver.get("https://id.atlassian.com/login/login");
	this.driver = driver;
    }

    // The login page contains several HTML elements that will be represented as WebElements.
    // The locators for these elements should only be defined once.
    By usernameLocator = By.id("username");
    By passwordLocator = By.id("password");
    By loginButtonLocator = By.id("login-submit");

    public Boolean isLoggedIn()
    {
	if (this.driver.getTitle().contains("Log in"))
	{
	    return false;
	}
	else
	{
	   return true;
	}
    }
    
    public LoginPage typeUsername(String username) {
	driver.findElement(usernameLocator).sendKeys(username);
	return this;    
    }

    public LoginPage typePassword(String password) {
	driver.findElement(passwordLocator).sendKeys(password);
	return this;    
    }

    public String submitLogin() {
	driver.findElement(loginButtonLocator).submit();
	return driver.getTitle();
    }


    public String loginAs(String username, String password) {
	typeUsername(username);
	typePassword(password);
	return submitLogin();
    }
}
