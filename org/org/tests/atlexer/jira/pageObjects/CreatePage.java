package org.tests.atlexer.jira.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreatePage {
    private WebDriver driver;
    
    public CreatePage(WebDriver driver) {
	this.driver = driver;
    }
    
    By projectLocator = By.id("project-field");
    By issueTypeLocator = By.id("issuetype-field");
    By summaryLocator = By.id("summary");
    By assigneeLocator = By.id("assignee-field");
    By descriptionLocator = By.id("description");
    By createLocator = By.id("create-issue-submit");
    By alertMsg = By.partialLinkText("has been successfully created");

    public CreatePage selectProject(String arg) {
	driver.findElement(projectLocator).sendKeys(arg);
	return this;    
    }

    public CreatePage selectAssignee(String arg) {
	driver.findElement(assigneeLocator).sendKeys(arg);
	return this;    
    }
    public CreatePage selectIssueType(String arg) {
	driver.findElement(issueTypeLocator).sendKeys(arg);
	return this;    
    }
    
    public CreatePage setSummary(String arg) {
	driver.findElement(summaryLocator).sendKeys(arg);
	return this;    
    }
    
    public CreatePage setDescription(String arg) {
	driver.findElement(descriptionLocator).sendKeys(arg);
	return this;    
    }
    
    public CreatePage submit()
    {
	driver.findElement(createLocator).submit();
	return this;
    }
    
    public String getBugId()
    {
	//<span class="aui-icon icon-success"></span>Issue <a class="issue-created-key issue-link" data-issue-key="TST-58089" href="/browse/TST-58089">TST-58089 - testtest</a> has been successfully created.<span class="aui-icon icon-close"></span>
	//WebElement element = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.className("issue-created-key")));	
	WebElement element = (new WebDriverWait(driver, 20)).until(ExpectedConditions.presenceOfElementLocated(By.className("issue-link")));	

	System.out.println (element.getText());
	return element.getText();
    }
    
    public CreatePage createBug(String proj, String issueType, String summary, String assignee) 
    {
	(new WebDriverWait(driver, 50)).until(ExpectedConditions.presenceOfElementLocated(By.id("summary")));	
	setSummary (summary);
	setDescription (summary);
	submit();
	return this;
    }
}
