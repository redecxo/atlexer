package org.tests.atlexer.jira.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    private final WebDriver driver;

    public HomePage(WebDriver driver) {
	this.driver = driver;
    }
    
    By createLocator = By.id("create_link");
    By searchLocator = By.id("quickSearchInput");

    public EditPage searchBug(String arg) {
	driver.findElement(searchLocator).sendKeys(arg);
	driver.findElement(searchLocator).submit();
	return (new EditPage (driver)); 
    }
    
    public CreatePage clickCreate() {
	driver.findElement(createLocator).click();
	return (new CreatePage(driver));   
    }
}
