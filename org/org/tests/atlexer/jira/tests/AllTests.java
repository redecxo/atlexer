package org.tests.atlexer.jira.tests;

import org.tests.atlexer.jira.util.Driver;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests 
{
   
    public void tearDown()
    {
	Driver.getDriver().close();
    }
    public static Test suite() {
	TestSuite suite = new TestSuite(AllTests.class.getName());
	suite.addTestSuite(CreateBugTest.class);
	suite.addTestSuite(SearchTest.class);
	suite.addTestSuite(EditTest.class);
	return suite;
    }
    
}
