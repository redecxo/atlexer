package org.tests.atlexer.jira.tests;

import org.openqa.selenium.WebDriver;
import org.tests.atlexer.jira.pageObjects.CreatePage;
import org.tests.atlexer.jira.pageObjects.HomePage;
import org.tests.atlexer.jira.pageObjects.LoginPage;
import org.tests.atlexer.jira.util.Driver;

import junit.framework.TestCase;	

public class CreateBugTest extends TestCase {
    private WebDriver driver;
    
    
    public CreateBugTest()
    {
	driver = Driver.getDriver(); //to Init the driver's singelton instance.

	driver.get(Cosntants.getString("loginPage")); 
	LoginPage lpage = new LoginPage (driver);

	if (lpage.isLoggedIn() == false)
	{
	   lpage.loginAs(Cosntants.getString("login"), Cosntants.getString("pwd"));  
	}
    }
    
    public void testLoading()
    {
	   driver.get(Cosntants.getString("projectPage"));
	   assertEquals (Cosntants.getString("projectPageTitle"), driver.getTitle());
    }
    
    public void testClickCreate()
    {
	HomePage hpage = new HomePage (driver);
	hpage.clickCreate();
	CreatePage cpage = new CreatePage(driver);
	cpage.createBug(
		Cosntants.getString("CreateBugTest.projectName"), 
		Cosntants.getString("CreateBugTest.issueType"), 
		Cosntants.getString("CreateBugTest.summary"), 
		Cosntants.getString("CreateBugTest.assignee"));
	
	assertTrue (cpage.getBugId().contains(Cosntants.getString("CreateBugTest.summary"))); 
    }
}
