package org.tests.atlexer.jira.tests;

import org.openqa.selenium.WebDriver;
import org.tests.atlexer.jira.pageObjects.HomePage;
import org.tests.atlexer.jira.pageObjects.LoginPage;
import org.tests.atlexer.jira.util.Driver;

import junit.framework.TestCase;	

public class SearchTest extends TestCase 
{
 
    private WebDriver driver;
    public SearchTest()
    {
	driver = Driver.getDriver(); //to Init the driver's singelton instance.

	driver.get(Cosntants.getString("loginPage"));  
	LoginPage lpage = new LoginPage (driver);

	if (lpage.isLoggedIn() == false)
	{
	   lpage.loginAs(Cosntants.getString("login"), Cosntants.getString("pwd"));   
	}
    }
    
    
    public void testSearchInValid()
    {
	driver.get(Cosntants.getString("projectPage")); 
	HomePage hpage = new HomePage (driver);
	hpage.searchBug(Cosntants.getString("SearchTest.wrongBugId")); 
	assertEquals(Cosntants.getString("SearchTest.wrongBugSearchTitle"), driver.getTitle());
    }
    
    public void testSearchvalid()
    {
	driver.get(Cosntants.getString("projectPage")); 
	HomePage hpage = new HomePage (driver);
	hpage.searchBug(Cosntants.getString("SearchTest.validBugId")); 
	assertEquals(Cosntants.getString("SearchTest.validBugURL"), driver.getCurrentUrl()); 
	assertEquals(Cosntants.getString("SearchTest.validBugPageTitle"), driver.getTitle()); 
    }
 }
