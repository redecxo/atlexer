package org.tests.atlexer.jira.tests;

import org.openqa.selenium.WebDriver;
import org.tests.atlexer.jira.pageObjects.EditPage;
import org.tests.atlexer.jira.pageObjects.HomePage;
import org.tests.atlexer.jira.pageObjects.LoginPage;
import org.tests.atlexer.jira.util.Driver;

import junit.framework.TestCase;	

public class EditTest extends TestCase 
{
    private WebDriver driver;   
    
    public EditTest()
    {
	driver = Driver.getDriver(); //to Init the driver's singelton instance.

	driver.get(Cosntants.getString("loginPage"));  //$NON-NLS-1$
	LoginPage lpage = new LoginPage (driver);

	if (lpage.isLoggedIn() == false)
	{
	   lpage.loginAs(Cosntants.getString("login"), Cosntants.getString("pwd"));   
	}
    }
    
    public void testEditBug()
    {
	driver.get(Cosntants.getString("projectPage"));
	HomePage hpage = new HomePage (driver);
	EditPage epage = hpage.searchBug(Cosntants.getString("EditTest.validBugId")); 
	epage.updateBug(
		Cosntants.getString("EditTest.summary"), 
		Cosntants.getString("EditTest.issueType"), 
		Cosntants.getString("EditTest.description"), 
		Cosntants.getString("EditTest.assignee")); 
	assertEquals(epage.getSummary(), Cosntants.getString("EditTest.updatedSummaryExpected")); 
    }
 }
