package org.tests.atlexer.jira.tests;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Cosntants {
    private static final String BUNDLE_NAME = "org.tests.atlexer.jira.tests.tests"; 

    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    private Cosntants() {
    }

    public static String getString(String key) {
	try {
	    return RESOURCE_BUNDLE.getString(key);
	} catch (MissingResourceException e) {
	    return '!' + key + '!';
	}
    }
}
